This program kind of sucks, but it's the best I could find that works for what we need. It's probably best to just leave it running so you don't have to select all the locations every time.

1. Open ExcelDocConverter.exe
2. On the dropdown, select csv - Comma Separated Value
3. Click Select a folder, and select repository\Unity Project\Assets\Resources\CSVs.
4. Click Select Files and select all the .xlsx files in repository\Level Sheets\.
5. Click Start Covert.
6. After testing, don't forget to check in all changed files!