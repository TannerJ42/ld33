﻿using UnityEngine;
using System.Collections.Generic;

public class GridWalker : MonoBehaviour 
{
    public float walkSpeed = 4;
    public float runSpeed = 6;

    public int leashDistance = 7;

    public State state = State.Idle;
    Instructions[] instructions;
    List<Instructions> chaseInstructions;
    List<Instructions> returnInstructions;
    List<Instructions> fightInstructions;
    int instructionsIndex;
    List<Instructions> chasePath;
    Grid targetGrid;

    Instructions currentInstruction = Instructions.Idle;

    GameObject chaseObject;
    Grid chaseObjectPosition;

    GridWalker fightPartner;
    Grid fightGrid;

	// Use this for initialization
	void Start() 
    {

	}

    void FixedUpdate()
    {
        switch (state)
        {
            case State.Idle:
                break;
            case State.Returning:
            case State.PathingToTrainer:
            case State.Pathing:
                MoveTowardTarget(walkSpeed);
                break;
            case State.Chasing:
                MoveTowardTarget(runSpeed);
                break;
        }
    }

    private void MoveTowardTarget(float moveSpeed)
    {
        if (targetGrid == null)
            return;

        Vector3 pos = transform.position;
        float distance = moveSpeed * Time.fixedDeltaTime;

        if (distance >= Vector2.Distance(pos, targetGrid.position))
        {
            transform.position = new Vector3(targetGrid.position.x, targetGrid.position.y, pos.z);
            OnReachTarget();
            return;
        }

        Vector3 direction = targetGrid.position - pos;

        transform.position = transform.position + moveSpeed * direction * Time.fixedDeltaTime;
    }

    private void OnReachTarget()
    {
        targetGrid = null;
        if (state == State.Pathing)
            instructionsIndex += 1;
        OnAIUpdated();
    }

    public void UpdateAI(Instructions[] inst, int startingInstruction)
    {
        instructions = inst;
        instructionsIndex = startingInstruction;
        OnAIUpdated();
    }

    private void OnAIUpdated()
    {
        if (instructions == null ||
            instructions.Length == 0)
        {
            currentInstruction = Instructions.Idle;
            return;
        }

        instructionsIndex = instructionsIndex % instructions.Length;

        switch (state)
        {
            case State.Idle:
            case State.Pathing:
                currentInstruction = instructions[instructionsIndex];
                break;
            case State.Chasing:
                if (chaseInstructions.Count <= 0 || returnInstructions.Count >= leashDistance)
                {
                    currentInstruction = Instructions.Idle;
                    chaseObject = null;
                    Invoke("returnToPath", 1.0f);
                    break;
                }
                Instructions i = chaseInstructions[0];
                chaseInstructions.RemoveAt(0);
                currentInstruction = i;
                returnInstructions.Add(getOppositeInstruction(i));
                break;
            case State.PathingToTrainer:
                if (!fightPartner)
                {
                    currentInstruction = Instructions.Idle;
                    fightPartner = null;
                    Invoke("returnToPath", 1.0f);
                }
                if (Vector2.Distance(transform.position, fightPartner.transform.position) <= 1 ||
                    fightInstructions.Count == 0)
                {
                    state = State.Fighting;
                    fightPartner.state = State.Fighting;
                    break;
                }

                Instructions f = fightInstructions[0];
                fightInstructions.RemoveAt(0);
                currentInstruction = f;
                returnInstructions.Add(getOppositeInstruction(f));

                break;
            case State.Returning:
                if (returnInstructions.Count <= 0)
                {
                    state = State.Pathing;
                    OnAIUpdated();
                    chaseInstructions = null;
                    returnInstructions = null;
                    break;
                }
                Instructions inst = returnInstructions[returnInstructions.Count - 1];
                returnInstructions.RemoveAt(returnInstructions.Count - 1);
                currentInstruction = inst;
                break;
        }
        
        processMoveInstruction();
        LookTowardTarget();
    }

    private void LookTowardTarget()
    {
        Vector3 direction = Direction.None;

        switch (currentInstruction)
        {
            case Instructions.Idle:
                break;
            case Instructions.ML:
            case Instructions.LL:
                direction = Direction.Left;
                break;
            case Instructions.MR:
            case Instructions.LR:
                direction = Direction.Right;
                break;
            case Instructions.MD:
            case Instructions.LD:
                direction = Direction.Down;
                break;
            case Instructions.MU:
            case Instructions.LU:
                direction = Direction.Up;
                break;
        }

        if (direction != Direction.None)
        {
            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }

    private void processMoveInstruction()
    {
        bool move = true;
        Vector2 currentGrid = GetCurrentGrid().position;

        switch (currentInstruction)
        {
            case Instructions.MU:
                currentGrid.y += 1;
                break;
            case Instructions.MD:
                currentGrid.y -= 1;
                break;
            case Instructions.ML:
                currentGrid.x -= 1;
                break;
            case Instructions.MR:
                currentGrid.x += 1;
                break;
            default:
                move = false;
                break;
        }
        if (move)
        {
            if (state == State.Idle)
                state = State.Pathing;
            targetGrid = new Grid(currentGrid);
        }
    }

    void SnapToGrid()
    {
        Vector2 gridPos = GetCurrentGrid().position;
        transform.position = new Vector3(gridPos.x, gridPos.y, transform.position.z);
    }

    void Update()
    {
        if (chaseObject)
        {
            Grid grid = new Grid(chaseObject.transform.position);
            if (grid.position != chaseObjectPosition.position)
            {
                Instructions i = getInstructionFromGrids(chaseObjectPosition, grid);
                Debug.Log("Adding instruction: " + i);
                chaseInstructions.Add(i);
                chaseObjectPosition = grid;
            }
        }
    }

    private Instructions getInstructionFromGrids(Grid chaseObjectPosition, Grid grid)
    {
        Vector2 direction = grid.position - chaseObjectPosition.position;
        Debug.Log("getInstructionFromGrids with direction " + direction);
        if (direction.x == -1)
            return Instructions.ML;
        if (direction.x == 1)
            return Instructions.MR;
        if (direction.y == -1)
            return Instructions.MD;
        if (direction.y == 1)
            return Instructions.MU;

        return Instructions.Idle;
    }

    public Grid GetCurrentGrid()
    {
        return new Grid(transform.position);
    }

    private void returnToPath()
    {
        state = State.Returning;
        OnAIUpdated();
    }

    public enum State
    {
        Idle,
        Pathing,
        Chasing,
        Returning,
        PathingToTrainer,
        Fighting,
        WaitingForFight,
    }

    public void startChasingPlayer(GameObject playerObject)
    {
        chaseObject = playerObject;
        chaseObjectPosition = new Grid(playerObject.transform.position);
        Grid playerGrid = new Grid(playerObject.transform.position);

        chaseInstructions = new List<Instructions>();
        returnInstructions = new List<Instructions>();

        Vector2 position = GetCurrentGrid().position;
        Vector2 difference = playerGrid.position - GetCurrentGrid().position;

        while (difference.x > 0)
        {
            chaseInstructions.Add(Instructions.MR);
            difference.x -= 1;
        }

        while (difference.x < 0)
        {
            chaseInstructions.Add(Instructions.ML);
            difference.x += 1;
        }
        while (difference.y > 0)
        {
            chaseInstructions.Add(Instructions.MU);
            difference.y -= 1;
        }
        while (difference.y > 0)
        {
            chaseInstructions.Add(Instructions.MD);
            difference.y -= 1;
        }

        state = State.Chasing;
        OnAIUpdated();
    }

    private Instructions getOppositeInstruction(Instructions i)
    {
        Instructions ret = Instructions.Idle;

        switch (i)
        {
            case Instructions.MR:
                ret = Instructions.ML;
                break;
            case Instructions.ML:
                ret = Instructions.MR;
                break;
            case Instructions.MU:
                ret = Instructions.MD;
                break;
            case Instructions.MD:
                ret = Instructions.MU;
                break;
        }
        return ret;
    }

    public void startFightingTrainer(GameObject trainer)
    {
        if (state == State.Fighting || state == State.WaitingForFight)
            return;

        GridWalker opponent = trainer.GetComponentInChildren<GridWalker>();

        if (!opponent || !opponent.onTrainerWantsToFight(this))
            return;

        SnapToGrid();
        opponent.SnapToGrid();

        fightPartner = opponent;

        state = State.PathingToTrainer;

        Grid myGrid = GetCurrentGrid();
        Grid theirGrid = opponent.GetCurrentGrid();

        Vector2 difference = theirGrid.position - myGrid.position;
        Debug.Log("Difference: " + difference);
        fightInstructions = new List<Instructions>();
        while (difference.x > 0)
        {
            fightInstructions.Add(Instructions.MR);
            difference.x -= 1;
        }

        while (difference.x < 0)
        {
            fightInstructions.Add(Instructions.ML);
            difference.x += 1;
        }
        while (difference.y > 0)
        {
            fightInstructions.Add(Instructions.MU);
            difference.y -= 1;
        }
        while (difference.y > 0)
        {
            fightInstructions.Add(Instructions.MD);
            difference.y -= 1;
        }

        Debug.Log("Will walk steps: " + fightInstructions.Count);

        if (returnInstructions == null)
            returnInstructions = new List<Instructions>();

        OnAIUpdated();
    }

    private bool onTrainerWantsToFight(GridWalker gridWalker)
    {
        if (state == State.PathingToTrainer || state == State.Fighting || state == State.WaitingForFight)
            return false;

        Vector2 direction = new Grid(gridWalker.transform.position).position - new Grid(this.transform.position).position;

        float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

        state = State.WaitingForFight;
        return true;
    }
}

public class Grid
{
    public Vector3 position;

    public Grid(Vector2 pos)
    {
        int x = Mathf.RoundToInt(pos.x);
        int y = Mathf.RoundToInt(pos.y);
        position = new Vector2(x, y);
    }
}

public enum Instructions
{
    Idle,
    MR,
    ML,
    MD,
    MU,
    LR,
    LL,
    LD,
    LU,
    THROW,
}