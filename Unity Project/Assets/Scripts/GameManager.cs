﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
    public static Dictionary<string, Entity> entities = new Dictionary<string, Entity>();
    public static Dictionary<string, Instructions[]> paths = new Dictionary<string, Instructions[]>();

    public float playerSeenPauseTime = 1f;

    public static GameManager instance;

    public static string levelToLoad = "";

    public static bool Loading = false;



    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

	// Use this for initialization
	void Start () 
    {
        instance = this;
        LoadPaths();
        LoadEntities();
	}
	
    void OnLevelWasLoaded(int i)
    {

        if (Loading == true)
        if (Application.loadedLevelName == "gameplay")
        {
            LoadLevel(levelToLoad);
        }

        Loading = false;
    }

	// Update is called once per frame
	void Update () 
    {
	    
	}

    public static void StartLoadingLevel(string level)
    {

        Loading = true;
        levelToLoad = level;
    }

    internal static void OnBallHitMonster(BallController ballController)
    {
        Debug.Log("Ball hit monster!");
    }

    public static void LoadPaths()
    {
        try
        {
            TextAsset pathsCSV = Resources.Load("CSVs/Paths") as TextAsset;
            string text = pathsCSV.text;

            while (text.IndexOf('\n') > -1)
                text = text.Remove(text.IndexOf('\n'), 1);

            string[] rows = text.Split('\r');

            for (int r = 0; r <= rows.Length -1; r++)
            {
                string[] s = rows[r].Split(',');

                if (s.Length <= 1)
                    continue;

                string key = s[0];
                List<Instructions> instructions = new List<Instructions>();

                for (int p = 1; p <= s.Length -1; p++)
                {
                    int count = 1;
                    String inst = s[p];
                    if (inst.Length > 2)
                    {
                        count = Int32.Parse(inst.Substring(2));
                        inst = inst.Substring(0, 2);
                    }
                    for (int i = 0; i < count; i++)
                        instructions.Add((Instructions)Enum.Parse(typeof(Instructions), inst));
                }

                paths[key] = instructions.ToArray();

            }
        }
        catch (Exception e)
        {
            Debug.Log("Couldn't load paths file");
            Debug.Log(e.Message);
        }
    }

    public static void LoadEntities()
    {
        TextAsset levelCSV = Resources.Load("CSVs/Key") as TextAsset;
        string text = levelCSV.text;

        while (text.IndexOf('\n') > -1)
            text = text.Remove(text.IndexOf('\n'), 1);

        string[] rows = text.Split('\r');
        for (int r = 1; r <= rows.Length - 1; r++)
        {
            string[] data = rows[r].Split(',');
            if (data.Length >= 6)
            {
                Entity entity = new Entity(data);
                entities[entity.ID] = entity;
            }
        }

    }

    public static void LoadLevel(string levelName)
    {
        Debug.Log("Loading level " + levelName);
        TextAsset levelCSV = Resources.Load("CSVs/" + levelName) as TextAsset;
        string text = levelCSV.text;

        while (text.IndexOf('\n') > -1)
            text = text.Remove(text.IndexOf('\n'), 1);

        string[] rows = text.Split('\r');
        for (int y = 0; y <= rows.Length - 1; y++)
        {
            string[] rowData = rows[y].Split(',');

            for (int x = 0; x <= rowData.Length - 1; x++)
            {
                if (rowData[x] != "")
                {
                    Vector2 location = new Vector2(x, -y);

                    if (entities.ContainsKey(rowData[x]))
                    {
                        Entity entity = entities[rowData[x]];
                        GameObject go = (GameObject)Instantiate(Resources.Load("Entities/" + entity.PrefabName), location, Quaternion.identity);
                        GridWalker gw = go.GetComponentInChildren<GridWalker>();
                        if (gw)
                        {
                            gw.UpdateAI(paths[entity.Path], entity.StartingIndex);
                        }
                        MonsterController mc = go.GetComponentInChildren<MonsterController>();
                        if (mc)
                        {
                            Vector3 pos = Camera.main.transform.position;
                            pos.x = go.transform.position.x;
                            pos.y = go.transform.position.y;
                            Camera.main.transform.position = pos;
                            Camera.main.transform.parent = go.transform;
                        }

                    }
                    else
                        Debug.Log("Couldn't spawn ID " + rowData[x] + " at location " + location);
                }
            }
        }
    }

    public struct Entity
    {
        public string ID;
        public string PrefabName;
        public string Name;
        public string Path;
        public int StartingIndex;
        public int StartingRotation;

        public Entity(string[] data)
        {
            ID = data[0];
            PrefabName = data[1];
            if (data[2] != "")
                Name = data[2];
            else
                Name = PrefabName;
            Path = data[3];
            if (data[4] != "")
                StartingIndex = Int32.Parse(data[4]);
            else
                StartingIndex = 0;
            if (data[5] == "R")
                StartingRotation = 90;
            else if (data[5] == "L")
                StartingRotation = -90;
            else if (data[5] == "D")
                StartingRotation = 180;
            else
                StartingRotation = 0;
        }
    }

    internal static void onPlayerSeen(GameObject gameObject)
    {
        instance.StartCoroutine("Pause", 0.5f);
    }


    IEnumerator Pause(float time)
    {
        Time.timeScale = 0f;
        float pauseEndTime = Time.realtimeSinceStartup + time;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        Time.timeScale = 1;
    }

    internal static void onTrainerSeen(GameObject gameObject)
    {
        instance.StartCoroutine("Pause", 0.5f);
    }
}