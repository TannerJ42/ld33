﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour 
{
    public string LevelToLoad = "";

	// Use this for initialization
	void Start () 
    {
        TextMesh tm = GetComponentInChildren<TextMesh>();
        tm.text = this.name;
        LevelToLoad = this.name;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    void OnMouseDown()
    {
        GameManager.StartLoadingLevel(LevelToLoad);
        Application.LoadLevel("gameplay");
    }
}
