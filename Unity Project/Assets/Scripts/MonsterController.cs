﻿using UnityEngine;
using System.Collections;

public class MonsterController : MonoBehaviour 
{
    public GameObject sprite;
    public float walkSpeed = 3f;

	// Use this for initialization
	void Start() 
    {
	    
	}

    void FixedUpdate()
    {
        Vector3 direction = getCurrentDirection();

        Vector3 movement = transform.position + walkSpeed * direction * Time.fixedDeltaTime;

        if (direction != Direction.None)
        {
            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            sprite.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }

        if (direction.x != 0)
        {
            movement.y = Mathf.RoundToInt(movement.y);
        }
        if (direction.y != 0)
        {
            movement.x = Mathf.RoundToInt(movement.x);
        }

        transform.position = movement;
    }

    private Vector3 getCurrentDirection()
    {
        if (Input.GetAxis("Horizontal") < 0)
            return Direction.Left;
        if (Input.GetAxis("Horizontal") > 0)
            return Direction.Right;
        if (Input.GetAxis("Vertical") > 0)
            return Direction.Up;
        if (Input.GetAxis("Vertical") < 0)
            return Direction.Down;

        return Direction.None;
    }
}

public class Direction
{
    public static Vector3 None = Vector2.zero;
    public static Vector3 Up = new Vector2(0, 1);
    public static Vector3 Down = new Vector2(0, -1);
    public static Vector3 Left = new Vector2(-1, 0);
    public static Vector3 Right = new Vector2(1, 0);
}
