﻿using UnityEngine;
using System.Collections;

public class TrainerController : MonoBehaviour 
{
    State state = State.Idle;
    public GameObject ballPrefab;
    public GameObject ballThrowLocation;
    public GameObject lineOfSightObject;
    GameObject currentTarget;
    Vector2 currentWaypoint;
    float stopDistance = 3.0f;
    public float timeBetweenBalls = 3f;

    float ballRefreshTime = 0;
    GridWalker gridWalker;

    float startingDetectionScale;

	// Use this for initialization
	void Start() 
    {
        gridWalker = GetComponentInChildren<GridWalker>();
        startingDetectionScale = lineOfSightObject.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update() 
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(lineOfSightObject.transform.position, transform.rotation * Vector2.up);

        for (int i = 0; i <= hits.Length - 1; i++)
        {
            if (hits[i].collider &&
                hits[i].collider.gameObject &&
                hits[i].collider.gameObject.tag == "Environment" &&
                hits[i].distance < startingDetectionScale)
            {
                Vector3 s = lineOfSightObject.transform.localScale;
                s.x = hits[i].distance;
                lineOfSightObject.transform.localScale = s;
                return;
            }
        }

        Vector3 n = lineOfSightObject.transform.localScale;
        n.x = startingDetectionScale;
        lineOfSightObject.transform.localScale = n;
	}

    void FixedUpdate()
    {
        if (gridWalker.state == GridWalker.State.Chasing)
        {
            ThrowBall();
        }
    }

    private void ThrowBall()
    {
        if (Time.timeSinceLevelLoad <= ballRefreshTime)
        {
            return;
        }
        ballRefreshTime = Time.timeSinceLevelLoad + timeBetweenBalls;
        Vector2 direction = currentTarget.transform.position - transform.position;
        direction.Normalize();
        BallController bc = ((GameObject)Instantiate(ballPrefab, ballThrowLocation.transform.position, transform.rotation)).GetComponent<BallController>();
        bc.Throw(direction);
    }

    private enum State
    {
        Idle,
        Chasing,
        Fighting,
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (!c || !c.gameObject)
            return;
        if (c.tag == "Player")
            onSeePlayer(c.gameObject);
        else if (c.tag == "Trainer")
            onSeeTrainer(c.gameObject);
        else
            return;
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (!c.gameObject)
            return;
        if (c.gameObject.tag == "Player")
            onTouchPlayer(c.gameObject);
        else if (c.gameObject.tag == "Trainer")
            onTouchTrainer(c.gameObject);
        else
            return;
    }

    private void onTouchTrainer(GameObject trainer)
    {
        
    }

    private void onTouchPlayer(GameObject player)
    {

    }

    private void onSeeTrainer(GameObject trainer)
    {
        if (gridWalker.state == GridWalker.State.Fighting ||
            gridWalker.state == GridWalker.State.PathingToTrainer ||
            gridWalker.state == GridWalker.State.WaitingForFight)
        {
            return;
        }

        GameManager.onTrainerSeen(this.gameObject);

        Debug.Log("onSeeTrainer");
        currentTarget = trainer;
        state = State.Fighting;
        gridWalker.startFightingTrainer(trainer);
    }

    private void onSeePlayer(GameObject player)
    {
        if (gridWalker.state == GridWalker.State.Fighting ||
    gridWalker.state == GridWalker.State.PathingToTrainer ||
    gridWalker.state == GridWalker.State.WaitingForFight)
        {
            return;
        }
        if (state == State.Chasing)
            return;
        GameManager.onPlayerSeen(this.gameObject);

        Debug.Log("onSeePlayer");
        currentTarget = player;
        state = State.Chasing;
        gridWalker.startChasingPlayer(player);
    }
}

public enum TrainerAI
{
    Idle,
    Rotate
}
