﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour 
{
    public float moveSpeed = 7;
    private Vector2 direction;

	// Use this for initialization
	void Start() 
    {
	    
	}

    void FixedUpdate()
    {
        transform.position = transform.position + (Vector3)direction * moveSpeed * Time.fixedDeltaTime;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.isTrigger || c.gameObject == null)
            return;

        if (c.gameObject.tag == "Player")
        {
            MonsterController mc = c.gameObject.GetComponent<MonsterController>();
            GameManager.OnBallHitMonster(this);
            Destroy(this.gameObject);
        }
        else if (c.gameObject.tag == "Environment")
        {
            Destroy(this.gameObject);
        }
    }

    public void Throw(Vector2 direction)
    {
        this.gameObject.SetActive(true);
        this.direction = direction;
    }
}
